import { withPluginApi } from 'discourse/lib/plugin-api';

export default {
  name: "extend-post",

  initialize(container) {
    withPluginApi('0.2', api => {

      Ember.View.reopen({
        didInsertElement: function() {
          this._super();
          if(this.element && this.element.id == "topic-closing-info") {
            var topic = $('#topic');
            var topic_id = topic.data('topic-id');
            var group = Discourse.SiteSettings.group;

            $.ajax("/highlight_post", {
              type: 'GET',
              data: { topic_id: topic_id, group: group }
            }).done(function(res){
              if(res.highlight_post){
                api.preventCloak(res.post_id);
                $("article.boxed[data-post-id='"+ res.post_id +"']").prepend('<div class="top-answer"><i class="fa fa-star" aria-hidden="true"></i><span class="top-answer-text">Top Answer</span></div>');
              }
            });
          }
        }
      });
    });
  }
};